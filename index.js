require('dotenv').config();

const TelegramBot = require('node-telegram-bot-api');
const puppeteer = require('puppeteer');
const { unlink } = require('fs');

const token = process.env.BOT_TOKEN;
const bot = new TelegramBot(token, {polling: true});

const IMAGE_PATH = 'table.png';

const resourceUrls = {
	wood: 'http://4.bp.blogspot.com/_IRn3d10CVhg/R7SHph6bKyI/AAAAAAAAAAc/RQeTeTakgkU/s320/Wood_Icon.png', 
	brick: 'http://www.clker.com/cliparts/y/D/l/y/y/O/brick-icon-hi.png',
	wheat: 'http://www.myiconfinder.com/uploads/iconsets/256-256-67257436e13aabced98091d45e727597-wheat.png', 
	sheep: 'http://www.iconarchive.com/download/i86443/martin-berube/flat-animal/sheep.ico', 
	stone: 'https://cdn.iconscout.com/icon/free/png-256/stone-11-449918.png', 
};
const buildings = {
	road: {
		cost: 2,
		url: 'http://jekabpilsnovads.lv/wp-content/uploads/2017/07/ico-road.png',
	},
	village: {
		cost: 4,
		url: 'http://icons.iconarchive.com/icons/custom-icon-design/flatastic-8/512/Home-icon.png',
	},
	city: {
		cost: 5,
		url: 'http://icons.iconarchive.com/icons/aha-soft/standard-city/256/city-icon.png',
	},
	progress: {
		cost: 3,
		url: 'https://images.vexels.com/media/users/3/152593/isolated/preview/d6368d8155eb832733a200df87f48e92-purple-circle-question-mark-icon-by-vexels.png',
	},
}
const resources = Object.keys(resourceUrls);
const urls = Object.assign({}, resourceUrls);
Object.keys(buildings).forEach(key => {
	urls[key] = buildings[key].url;
});

let bussy = false;

bot.onText(/\/ping/, msg => {
	bot.sendMessage(msg.chat.id, 'Hello! I\'m still working :D');
})

bot.onText(/\/costs/, msg => {

	bussy = true;
	console.log('Received request! Creating new resources table...');
	createCostsTable()
	.then(() => {
		console.log('Closed! Sending photo');
		return bot.sendPhoto(msg.chat.id, IMAGE_PATH)
	})
	.then(() => {
		console.log('Sent! Removing picture');
		return new Promise((resolve, reject) => {
			unlink(IMAGE_PATH, error => {
				if(error) reject(error);
				else resolve();
			});
		});
	})
	.then(() => {
		console.log('Successfully finished!');
	})
	.catch(error => {
		console.log('We have found an error :(');
		console.log(error);
	})
	.finally(() => {
		console.log('Finishing...');
		bussy = false;
	});

});

function createCostsTable() {
	const html = generateHTML();

	return Promise.resolve()
	.then(() => {
		console.log('Launching puppeteer');
		return puppeteer.launch();
	})
	.then(browser => {
		console.log('Launched! now creating new page');
		return Promise.all([
			Promise.resolve(browser),
			browser.newPage(),
		]);
	})
	.then(([browser, page]) => {
		console.log('Created! Setting content');
		return Promise.all([
			Promise.resolve(browser),
			Promise.resolve(page),
			page.setContent(html),
		]);
	})
	.then(([browser, page]) => {
		console.log('Set! Saving screenshot');
		return Promise.all([
			Promise.resolve(browser),
			page.screenshot({path: IMAGE_PATH}),
		]);
	})
	.then(([browser]) => {
		console.log('Saved! Closing browser');
		return browser.close();
	});
}

function sortResourcesFunction(resource1, resource2) {
	return resources.indexOf(resource1) - resources.indexOf(resource2);
}

function getRandomResources(n) {
	return new Array(n).fill('').map(() => resources[parseInt(Math.random()*resources.length)]).sort(sortResourcesFunction);
}

function buildTable() {
	const result = {};
	Object.keys(buildings).forEach(key => {
		result[key] = getRandomResources(buildings[key].cost);
	});
	return result;
}

function generateHTML() {

	const costTable = buildTable();
	console.log(costTable);

	return `
<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<style type="text/css">
			#container {
				width: 700px;
				height: 600px;
			}

			.buildingContainer {
				width: 700px;
				height: 150px;
				display: flex;
				justify-content: space-between;
				align-items: center;
			}

			.building {
				height: 150px;
				width: 150px;
			}

			.resource {
				height: 95px;
				width: 95px;
				padding: 0 5px;
			}

			.icon {
				background-repeat: no-repeat;
				background-position: center;
				background-size: contain;
				display: inline-block;
			}

			${Object.keys(urls).map(key => 
			`
			.${key} {
				background-image: url(${urls[key]});
			}
			`
			).join('\n\n')}
		</style>
	</head>
	<body>
		<div id="container">
			${Object.keys(costTable).map(key => 
			`
			<div class="buildingContainer">
				<div class="building icon ${key}"></div>
				<div class="resourceContainer">
					${costTable[key].map(resource => 
					`<div class="resource icon ${resource}"></div>`
					).join('')}
				</div>
			</div>
			`
			).join('\n')}
		</div>
	</body>
</html>
`;

}
